import React from 'react'
import { View, Text } from 'react-native'
import Splash from './Pages/Splash'
import Router from './Router'

export default function index() {
    return (
        // <Router />
        <Router />
    )
}
