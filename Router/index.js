// import 'react-native-gesture-handler';
import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';


import AboutScreen from '../Pages/About';
import LoginScreen from '../Pages/Login';
import HomeScreen from '../Pages/Home';
import Register from '../Pages/Register';
import Detail from '../Pages/Detail';
import DetailGlobal from '../Pages/DetailGlobal';

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();
export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="LoginScreen" component={LoginScreen} />
                <Stack.Screen name="HomeScreen" component={HomeScreen} />
                <Stack.Screen name="MainApp" component={MainApp} />
                <Stack.Screen name="Sicov Informasi Covid19" component={SicovApp} />
                <Stack.Screen name="RegisterScreen" component={Register} />
                <Stack.Screen name="Covid Indonesia" component={Detail} />
                <Stack.Screen name="Covid Global" component={DetailGlobal} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => (
    <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                if (route.name === 'HomeScreen') {
                    return (
                        <Ionicons
                            name={
                                focused
                                    ? 'home'
                                    : 'home-outline'
                            }
                            size={size}
                            color={color}
                        />
                    );
                } else if (route.name === 'AboutScreen') {
                    return (
                        <Ionicons
                            name={focused ? 'ios-call' : 'ios-call-outline'}
                            size={size}
                            color={color}
                        />
                    );
                }
                //  else if (route.name === 'Register') {
                //     return (
                //         <Ionicons
                //             name={focused ? 'ios-man' : 'man-outline'}
                //             size={size}
                //             color={color}
                //         />
                //     );
                // }

            },
        })}
        tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
            labelStyle: {
                fontSize: 14,
            },
            style: {
                // backgroundColor: '#fff', // Makes Android tab bar white instead of standard blue
                height: (Platform.OS === 'ios') ? 58 : 60 // I didn't use this in my app, so the numbers may be off. 
            }
        }}
    >
        <Tab.Screen name="HomeScreen" component={HomeScreen} />
        <Tab.Screen name="AboutScreen" component={AboutScreen} />
        {/* <Tab.Screen name="Register" component={Register} /> */}

    </Tab.Navigator>
)


const SicovApp = () => (
    <Drawwer.Navigator>
        <Drawwer.Screen name="App" component={MainApp} />
        <Drawwer.Screen name="AboutScreen" component={AboutScreen} />
    </Drawwer.Navigator>
)

// const Login = () => (
//     <Drawwer.Navigator>
//         <Drawwer.Screen name="LoginScreen" component={LoginScreen} />
//     </Drawwer.Navigator>

// )