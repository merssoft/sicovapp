import axios from 'axios';
import React, { useEffect, useState, Component } from 'react'
import { View, Text, SafeAreaView, FlatList, StyleSheet, Image, Button } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { Col, Row, Grid } from "react-native-easy-grid";

import { FlatGrid } from 'react-native-super-grid';
var randomColor = require('randomcolor');

export default function Home({ navigation }) {

    const [items, setItems] = useState("");
    const [itemsG, setItemsG] = useState("");
    const [positifG, setPositifG] = useState("");
    const [sembuhG, setSembuhG] = useState("");
    const [MatiG, setMatiG] = useState("");

    const GetData = () => {
        // setButton("Simpan")
        // axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
        axios.get('https://api.kawalcorona.com/indonesia')
            .then(res => {
                const data1 = (res.data)
                console.log('res: ', data1);
                setItems(data1)
                // console.log(items);
            })

    }
    const GetData2 = () => {
        // setButton("Simpan")
        // axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
        axios.get('https://api.kawalcorona.com/positif')
            .then(res => {
                const data1 = (res.data)
                console.log('res: ', data1);
                setPositifG(data1)
                // console.log(items);
            })
        axios.get('https://api.kawalcorona.com/sembuh')
            .then(res => {
                const data1 = (res.data)
                console.log('res: ', data1);
                setSembuhG(data1)
                // console.log(items);
            })
        axios.get('https://api.kawalcorona.com/meninggal')
            .then(res => {
                const data1 = (res.data)
                console.log('res: ', data1);
                setMatiG(data1)
                // console.log(items);
            })
        axios.get('https://api.kawalcorona.com/meninggal')
            .then(res => {
                const data1 = (res.data)
                console.log('res: ', data1);
                setMatiG(data1)
                // console.log(items);
            })
    }
    useEffect(() => {

        GetData();
        console.log(items);
        GetData2();
        console.log(positifG);
        console.log(sembuhG);
        console.log(MatiG);
        // (async () => {


    }, []);

    if (items === null) {
        return null;
    }
    // setTimeout(() => {

    // }, 3000) //TIME OF WAITING

    return (

        <View style={{
            flex: 1, backgroundColor: '#FDFDFD', alignContent: 'center'
        }}>
            <View style={{ paddingTop: 20, }}>
                <Image
                    style={{ width: 450, height: 150, }}
                    source={require('../assets/image3.png')}
                />
            </View>
            <View style={{
                flex: 1, justifyContent: 'center', backgroundColor: '#FDFDFD', height: 200, margin: 10, marginTop: 30,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.25,
                // shadowRadius: 3.84,

                elevation: 5,
            }}>
                {items.length > 0 ? (
                    <FlatGrid
                        itemDimension={250}
                        data={items}
                        style={styles.gridView}
                        // staticDimension={300}
                        // fixed
                        // spacing={30}
                        renderItem={({ item }) => (
                            // <Text style={styles.itemName}>{item.title}</Text>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Covid Indonesia', {
                                    screen: 'Covid Indonesia', params: {
                                        screen: 'Covid Indonesia'
                                    }
                                })}
                            >
                                <View style={[styles.itemContainer, { backgroundColor: 'white' }]}>
                                    {/* <Text style={styles.itemName}>TEst</Text> */}
                                    <Text style={styles.itemName}>{item.name}</Text>
                                    <Text style={styles.itemCode}>Konfirmasi : {item.positif}</Text>
                                    <Text style={styles.itemCodeg}>Sembuh : {item.sembuh}</Text>
                                    <Text style={styles.itemCoder}>Mati : {item.meninggal}</Text>
                                    <Text style={styles.itemCodeb}>Dirawat : {item.dirawat}</Text>
                                    <Text style={styles.itemCode}></Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                ) : (
                    <Text style={styles.itemName2}>Loading Data ..</Text>

                )}
            </View>
            <View style={{
                flex: 1, backgroundColor: '#FDFDFD', height: 200, margin: 10, marginBottom: 40,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.25,
                // shadowRadius: 3.84,

                elevation: 5,
            }}>
                {items.length > 0 ? (
                    <FlatGrid
                        itemDimension={250}
                        data={items}
                        style={styles.gridView}
                        // staticDimension={300}
                        // fixed
                        // spacing={30}
                        renderItem={({ item }) => (
                            // <Text style={styles.itemName}>{item.title}</Text>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Covid Global', {
                                    screen: 'Covid Global', params: {
                                        screen: 'Covid Global'
                                    }
                                })}
                            >
                                <View style={[styles.itemContainer, { backgroundColor: 'white' }]}>
                                    {/* <Text style={styles.itemName}>TEst</Text> */}
                                    <Text style={styles.itemName}>Global / Semua Negara</Text>
                                    <Text style={styles.itemCode}>Konfirmasi : {positifG.value}</Text>
                                    <Text style={styles.itemCodeg}>Sembuh : {sembuhG.value}</Text>
                                    <Text style={styles.itemCoder}>Mati : {MatiG.value}</Text>
                                    <Text style={styles.itemCode}></Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                ) : (
                    <Text style={styles.itemName2}>Loading Data ..</Text>

                )}
            </View>
        </View >

    )
}



const styles = StyleSheet.create({
    gridView: {
        marginTop: 10,
        flex: 1
    },
    itemContainer: {
        justifyContent: 'flex-end',
        borderRadius: 5,
        padding: 10,
        height: 200,
    },
    itemName: {
        fontSize: 34,
        color: 'black',
        fontWeight: '600',
    },
    itemName2: {
        fontSize: 14,
        color: 'black',
        fontWeight: '600',
    },
    itemCode: {
        fontWeight: '600',
        fontSize: 20,
        color: 'black',
    },
    itemCoder: {
        fontWeight: '600',
        fontSize: 20,
        color: 'red',
    },
    itemCodeg: {
        fontWeight: '600',
        fontSize: 20,
        color: 'green',
    },
    itemCodeb: {
        fontWeight: '600',
        fontSize: 20,
        color: 'blue',
    },
});
