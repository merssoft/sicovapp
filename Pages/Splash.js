import React from 'react';
import { StatusBar, View, Text, ActivityIndicator, Image } from 'react-native';
export default class SplashScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FDFDFD' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FDFDFD' }}>
                    <StatusBar backgroundColor="#2c3e50" barStyle="light-content" />

                    <Image
                        style={{ width: 195, height: 40 }}
                        source={require('../assets/SICOV.png')}
                    />
                    <Image
                        style={{ width: 195, height: 10, marginTop: 10 }}
                        source={require('../assets/ketsicov.png')}
                    />
                    {/* <Text style={{ color: 'black', fontSize: 45, fontFamily: '' }}>SICOV</Text> */}

                    <ActivityIndicator style={{ marginTop: 50 }} color={'black'} />
                </View>
                <View>
                    <Image
                        style={{ width: 450 }}
                        source={require('../assets/image1.png')}
                    />

                </View>

            </View>

        )
    }
}