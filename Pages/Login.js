import React, { useState } from 'react'
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity, Button } from 'react-native'

import * as firebase from 'firebase';

const Separator = () => (
    <View style={styles.separator} />
);
export default function Login({ navigation }) {
    const firebaseConfig = {
        apiKey: "AIzaSyDSdMA_5F1_t_1zmcrVuNsCP2Ak6B0zlT0",
        authDomain: "authenticationfirebasern-2b7ba.firebaseapp.com",
        databaseURL: "https://authenticationfirebasern-2b7ba-default-rtdb.firebaseio.com",
        projectId: "authenticationfirebasern-2b7ba",
        storageBucket: "authenticationfirebasern-2b7ba.appspot.com",
        messagingSenderId: "886958605342",
        appId: "1:886958605342:web:b4ddbdb498224cf8601a0f"
    };
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig)
    }
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const submit = () => {
        const data = {
            email,
            password
        }
        console.log(data)
        firebase.auth().signInWithEmailAndPassword(email, password).then(() => {
            console.log("berhasil login");
            navigation.navigate("Sicov Informasi Covid19", {
                screen: 'Sicov Informasi Covid19', params: {
                    screen: 'Sicov Informasi Covid19'
                }
            })
        }).catch(() => {
            console.log("Login gagal")
        })
    }
    return (
        <View style={styles.container}>


            <View style={styles.judul}>
                <Text style={{ fontSize: 24 }}>Silahkan login terlebih dahulu</Text>
            </View>
            {/* <View style={{ flex: 1, backgroundColor: 'black' }} /> */}
            <View style={styles.body}>

                <View style={styles.subbody}>

                    <Text style={styles.jarak}>Email</Text>
                    <TextInput
                        placeholder="Masukkan Email                                                                                                            "
                        style={styles.input}
                        value={email}
                        onChangeText={(value) => setEmail(value)}
                    />
                    <Text style={styles.jarak}>Password</Text>
                    <TextInput secureTextEntry={true}
                        placeholder="Masukkan password                                                                                                    "
                        style={styles.input}
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                    />

                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>

                        <Separator />
                        <Button onPress={submit} title="      Login      "
                        />

                        <Separator />
                        <Text style={styles.jarak}>
                            Atau</Text>
                        <Separator />

                        <Button
                            color="#00b894"
                            onPress={() => navigation.navigate("RegisterScreen")} title="      Daftar      "
                        />

                    </View>


                </View>

            </View>
            <View style={styles.body2}>
                <Image
                    style={{ width: 450 }}
                    source={require('../assets/image3.png')}
                />
            </View>

        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FDFDFD'
    },

    judul: {
        flexDirection: 'row',
        marginTop: 40,
        marginHorizontal: 50,
        alignItems: 'flex-start',

    },
    body: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 30,
        marginHorizontal: 30,
        // alignItems: 'flex-start',
        // justifyContent: 'center',
        backgroundColor: '#dfe4ea',
        // height: 277,
        borderRadius: 10
        // marginLeft: 50,
        // margin: 50,

    },
    subbody: {
        flex: 1,
        flexDirection: 'column',
        // marginTop: 50,
        // height: 50,
        margin: 20,
        // flexDirection: 'row',
        // alignItems: 'flex-start',
        // alignItems: 'center',
        justifyContent: 'flex-start',
        // backgroundColor: 'white',
        // height: 237,

        // margin:5
        // width: 303,
        // marginLeft: 50,

    },
    body2: {
        flexDirection: 'row',
        // marginTop: 10,
        // marginHorizontal: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fdcb6e',
        // height: 70,
        // borderRadius: 10,
        marginBottom: 20
        // marginLeft: 50,
        // margin: 50,

    },
    textbox: {
        width: 310, height: 40, borderColor: 'gray', borderWidth: 1,
        backgroundColor: 'white'
    },
    tombol: {
        height: 40,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 10,
        borderRadius: 5,
        fontSize: 16
    },
    tombol2: {
        height: 40,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20,
        marginBottom: 10,
        borderRadius: 5,
        fontSize: 16
    },
    jarak: {
        // marginTop: 10,
        marginBottom: 10,
        fontSize: 16,
        color: 'black',
    },
    logo: {
        height: 100,
        width: 140
    },
    input: {
        flex: 1,
        borderWidth: 1,
        paddingVertical: 5,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 20,
        alignSelf: "center",
        // width: 310, height: 40, 
        borderColor: 'gray', borderWidth: 1,
        backgroundColor: 'white',

    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#fdcb6e',
        borderBottomWidth: StyleSheet.hairlineWidth,
        padding: 1
    },

})