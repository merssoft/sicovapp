import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity, SafeAreaView, Button } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import * as firebase from 'firebase';

// const login = () => {
export default function Register({ navigation }) {

    const firebaseConfig = {
        apiKey: "AIzaSyDSdMA_5F1_t_1zmcrVuNsCP2Ak6B0zlT0",
        authDomain: "authenticationfirebasern-2b7ba.firebaseapp.com",
        databaseURL: "https://authenticationfirebasern-2b7ba-default-rtdb.firebaseio.com",
        projectId: "authenticationfirebasern-2b7ba",
        storageBucket: "authenticationfirebasern-2b7ba.appspot.com",
        messagingSenderId: "886958605342",
        appId: "1:886958605342:web:b4ddbdb498224cf8601a0f"
    };
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig)
    }

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [password2, setPassword2] = useState("");

    const submit = () => {
        const data = {
            email,
            password,
            password2
        }
        console.log(data);
        if (password == password2) {

            firebase.auth().createUserWithEmailAndPassword(email, password).then(() => {
                console.log('Register Berhasil');
                navigation.navigate("LoginScreen");
            }).catch((error) => {
                console.log("register gagal")
                console.log(error)
            })
        } else {
            console.log('Password Tidak Cocok')
        }

    }
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.scrollView}>

                <View style={styles.judul}>
                    <Text style={{ fontSize: 24 }}>Register</Text>
                </View>

                <View style={styles.body}>

                    <View style={styles.subbody}>
                        <Text style={styles.jarak}>Email</Text>
                        <TextInput
                            placeholder="Masukkan Email                                                                                                            "
                            style={styles.input}
                            value={email}
                            onChangeText={(value) => setEmail(value)}
                        />
                        <Text style={styles.jarak}>Password</Text>
                        <TextInput secureTextEntry={true}
                            placeholder="Masukkan password                                                                                                    "
                            style={styles.input}
                            value={password}
                            onChangeText={(value) => setPassword(value)}
                        />

                        <Text style={styles.jarak}>Ulangi Password</Text>
                        <TextInput secureTextEntry={true}
                            placeholder="Ulangi password                                                                                                    "
                            style={styles.input}
                            value={password2}
                            onChangeText={(value) => setPassword2(value)}
                        />

                    </View>

                </View>
                <View style={styles.body2}>
                    <Button onPress={() => navigation.navigate("LoginScreen", {
                        screen: 'LoginScreen', params: {
                            screen: 'LoginScreen'
                        }
                    })} title="        Login        " style={styles.tombol}
                        backgroundColor='#56CCF2'
                    />
                    <Text>       Atau       </Text>
                    <Button onPress={submit} title="      Daftar      " color="#6FCF97"
                    />


                </View>
                <View>
                    <Image
                        style={{ width: 450 }}
                        source={require('../assets/image3.png')}
                    />
                </View>

            </ScrollView >
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FDFDFD'
    },

    judul: {
        flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 50,
        alignItems: 'flex-start',

    },
    body: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 30,
        paddingBottom: 10,
        alignItems: 'flex-start',
        // justifyContent: 'center',
        backgroundColor: '#DFE6E9',
        // height: 420,
        // marginBottom:50,
        borderRadius: 10
        // marginLeft: 50,
        // margin: 50,

    },
    subbody: {
        // marginTop: 50,
        // height: 50,
        margin: 20,
        // flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        // backgroundColor: 'white',
        height: 337,
        // margin:5
        // width: 303,
        // marginLeft: 50,

    },
    body2: {
        flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#DFE6E9',
        height: 60,
        borderRadius: 10,
        marginBottom: 30
        // marginLeft: 50,
        // margin: 50,

    },
    textbox: {
        // flex: 1,
        borderWidth: 1,
        paddingVertical: 5,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 10,
        alignSelf: "center",
        // width: 310, height: 40, 
        borderColor: 'gray', borderWidth: 1,
        backgroundColor: 'white'
    },
    tombol: {
        height: 40,
        width: 162,
        alignItems: 'center',
        justifyContent: 'center',
        // marginTop: 20,
        // marginBottom:10,
        borderRadius: 5,
        fontSize: 16,
        // marginRight: 50
    },
    logo: {
        height: 100,
        width: 140
    },
    jarak: {
        // marginTop: 5,

        marginBottom: 10,
        fontSize: 16,
        color: 'black'
    },
    input: {
        flex: 1,
        borderWidth: 1,
        paddingVertical: 5,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 20,
        alignSelf: "center",
        // width: 310, height: 40, 
        borderColor: 'gray', borderWidth: 1,
        backgroundColor: 'white'
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#fdcb6e',
        borderBottomWidth: StyleSheet.hairlineWidth,
        padding: 1
    },

})