import axios from 'axios';
import React, { useEffect, useState, Component } from 'react'
import { View, Text, SafeAreaView, FlatList, StyleSheet, Image, Button } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { Col, Row, Grid } from "react-native-easy-grid";

import { FlatGrid } from 'react-native-super-grid';
var randomColor = require('randomcolor');

export default function Home({ navigation }) {

    const [items, setItems] = useState("");

    const GetData = () => {
        // setButton("Simpan")
        // axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
        axios.get('https://api.kawalcorona.com/indonesia/provinsi')
            .then(res => {
                const data1 = (res.data)
                console.log('res: ', data1);
                setItems(data1)
                // console.log(items);
            })

    }
    useEffect(() => {

        GetData();
        console.log(items);
        // (async () => {


    }, []);

    if (items === null) {
        return null;
    }
    // setTimeout(() => {

    // }, 3000) //TIME OF WAITING

    return (
        <View style={{
            flex: 1, backgroundColor: '#FDFDFD'
        }}>
            <View style={{ flex: 1, backgroundColor: '#FDFDFD', height: 150, margin: 10 }}>
                {items.length > 0 ? (
                    <FlatGrid
                        itemDimension={140}
                        data={items}
                        style={styles.gridView}
                        // staticDimension={300}
                        // fixed
                        spacing={10}
                        renderItem={({ item }) => (
                            // <Text style={styles.itemName}>{item.title}</Text>
                            <TouchableOpacity
                            // onPress={() => navigation.navigate(`${item.act}`, {
                            //     screen: `${item.act}`, params: {
                            //         screen: `${item.act}`
                            //     }
                            // })}
                            >
                                <View style={[styles.itemContainer, { backgroundColor: randomColor() }]}>
                                    {/* <Text style={styles.itemName}>TEst</Text> */}
                                    <Text style={styles.itemName}>{item.attributes.Provinsi}</Text>
                                    <Text style={styles.itemCode}>Konfirmasi : {item.attributes.Kasus_Posi}</Text>
                                    <Text style={styles.itemCode}>Sembuh : {item.attributes.Kasus_Semb}</Text>
                                    <Text style={styles.itemCode}>Mati : {item.attributes.Kasus_Meni}</Text>
                                    <Text style={styles.itemCode}>Aktif : {item.attributes.Kasus_Posi - item.attributes.Kasus_Semb - item.attributes.Kasus_Meni}</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    />
                ) : (
                    <Text style={styles.itemName2}>Loading Data ..</Text>

                )}
            </View>

        </View >

    )
}



const styles = StyleSheet.create({
    gridView: {
        marginTop: 10,
        flex: 1,
    },
    itemContainer: {
        justifyContent: 'flex-end',
        borderRadius: 5,
        padding: 10,
        height: 100,
    },
    itemName: {
        fontSize: 14,
        color: '#fff',
        fontWeight: '600',
    },
    itemName2: {
        fontSize: 14,
        color: 'black',
        fontWeight: '600',
    },
    itemCode: {
        fontWeight: '600',
        fontSize: 12,
        color: 'black',
    },
});
